package com.helloworld;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloworldController {
	@GetMapping(path="/")
	public String output() {
		return "Hello World!";
	}
}
